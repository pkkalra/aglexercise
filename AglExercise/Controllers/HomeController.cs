﻿using AglExercise.Service;
using AglExercise.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AglExercise.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult Cats()
        {
            return Json(GetCats(PeopleService.GetPeopleJson()), JsonRequestBehavior.AllowGet);
        }

        private CatInfo[] GetCats(List<Person> persons)
        {
            var result =  persons.Where(p => p.Pets !=null )
                .SelectMany(p => p.Pets.Where(pet => pet.Type == "Cat")
                    .Select(pet => new CatInfo()
                    {
                        Name = pet.Name,
                        OwnerGender = p.Gender.ToString()
                    })
                ).OrderBy(c => c.Name).ToArray();

            return result;
        }
    }

    class CatInfo
    {
        public string Name { get; set; }
        public string OwnerGender { get; set; }
    }
}