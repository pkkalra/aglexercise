﻿using System.Web;
using System.Web.Optimization;

namespace AglExercise
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                         "~/Scripts/jquery-1.10.2.min.js",
                          //"~/Scripts/bootstrap.min.js"
                        "~/Scripts/knockout-3.4.0.js",
                        "~/Scripts/util.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/site.css"));
        }
    }
}
