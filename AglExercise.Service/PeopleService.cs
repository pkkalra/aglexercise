﻿using AglExercise.Service.Helpers;
using AglExercise.Service.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AglExercise.Service
{
    public class PeopleService
    {
        public const string _peopleServiceUrl = @"http://agl-developer-test.azurewebsites.net/people.json";
        public static List<Person> GetPeopleJson()
        {
            string peopleJson = JsonWebServiceHelper.GET(_peopleServiceUrl);
            return JsonConvert.DeserializeObject<List<Person>>(peopleJson);
        }
    }
}
